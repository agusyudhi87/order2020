<?php

use App\Models\Sisfo\Student;
use App\Models\Sisfo\Program;
use App\Models\Auth\User;
use Faker\Generator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


 
$factory->define(Student::class, function (Generator $faker) {
    $users = User::find([6, 7, 8, 9,10])->pluck('id')->toArray();
    $programs = Program::find([6, 7, 8, 9,10])->pluck('id')->toArray();
    //dd($programs);
   // dd($users);
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'code' => Str::random(6),
        'fullname' => $faker->name($gender),
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'photo' => 'Student.jpg',
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'gender' => ($gender=='male')?'L':'P',
        'user_id' => $faker->unique()->randomElement($users),
        'program_id' => $faker->randomElement($programs),
        'gpa' => 0,
        'status' => '1',        
    ];
});
