<?php

use App\Models\Sisfo\Employee;
use App\Models\Auth\User;
use Faker\Generator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/



$factory->define(Employee::class, function (Generator $faker) {
    $users = User::find([2, 3, 4, 5])->pluck('id')->toArray();
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'code' => Str::random(6),
        'fullname' => $faker->name($gender),
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'photo' => 'Employee.jpg',
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'gender' => ($gender=='male')?'L':'P',
        'user_id' => $faker->unique()->randomElement($users),
        'type' => '1',
        'status' => '1',        
    ];
});
