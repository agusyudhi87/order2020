<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;
use App\Models\Auth\User;
/**
 * Class AuthTableSeeder.
 */
class AuthFakerTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Reset cached roles and permissions
        resolve(PermissionRegistrar::class)->forgetCachedPermissions();


        // factory(User::class,9)->create();  
        // $this->call(UserFakerRoleTableSeeder::class);

        $this->enableForeignKeys();
    }
}
