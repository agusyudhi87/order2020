<?php

use App\Models\OrderMgmt\Product;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class ProductTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        Product::create([
            'name' =>'Scrub',
            'unit_id' => 1,
            'price' => '20000',
            'stock' =>'5', 
        ]);
        // Add the master administrator, user id of 1
        Product::create([
            'name' =>'Handbody Lotion',
            'unit_id' => 1,
            'price' => '20000',
            'stock' =>'10', 
        ]);


        $this->enableForeignKeys();
    }
}
