<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Models\Auth\User;

class DatabaseSeeder extends Seeder
{
    use TruncateTable;

    /**
     * Seed the application's database.
     */
    public function run()
    {
        Model::unguard();

        $this->truncateMultiple([
            'cache',
            'failed_jobs',
            'ledgers',
            'jobs',
            'sessions',
        ]);

        $this->call(AuthTableSeeder::class); 
        $this->call(ProductTableSeeder::class); 
        
        // $this->call(FacultyTableSeeder::class);       
        // $this->call(ProgramTableSeeder::class); 
      
        Model::reguard();
    }
}
