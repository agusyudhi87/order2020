<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.admin_role'));
        User::find(2)->assignRole(config('access.users.produsen_role'));
        User::find(3)->assignRole(config('access.users.distributor_role'));
        User::find(4)->assignRole(config('access.users.agent_role'));

        

        // User::find([2, 3, 4, 5])->assignRole(config('access.users.employee_role'));
        // User::find([6, 7, 8, 9])->assignRole(config('access.users.student_role'));

        $this->enableForeignKeys();
    }
}
