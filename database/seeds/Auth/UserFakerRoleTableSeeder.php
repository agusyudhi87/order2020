<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserFakerRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();


        $idusers = User::find([2, 3])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.lecture_role'));
        }            

        $idusers = User::find([4, 5])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.employee_role'));
        }        

        $idusers = User::find([6, 7, 8, 9, 10])->pluck('id')->toArray();
        foreach ($idusers as $iduser) {
            User::find($iduser)->assignRole(config('access.users.student_role'));
        }


        $this->enableForeignKeys();
    }
}
