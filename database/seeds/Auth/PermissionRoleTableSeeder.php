<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        Role::create(['name' => config('access.users.admin_role')]);
        Role::create(['name' => config('access.users.produsen_role')]);
        Role::create(['name' => config('access.users.distributor_role')]);
        Role::create(['name' => config('access.users.agent_role')]);

        // Create Permissions
        Permission::create(['name' => 'view backend']);

        // $permissions = [
        //    'student-list',
        //    'student-create',
        //    'student-edit',
        //    'student-delete',
        //    'employee-list',
        //    'employee-create',
        //    'employee-edit',
        //    'employee-delete'
        // ];

        // foreach ($permissions as $permission) {
        //      Permission::create(['name' => $permission]);
        // }
        
        // Assign Permissions to other Roles
        // Note: Admin (User 1) Has all permissions via a gate in the AuthServiceProvider
        // $user->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
