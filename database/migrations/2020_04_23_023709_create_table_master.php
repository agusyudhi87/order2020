<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMaster extends Migration
{
	public function up()
	{
		// create produsens
		Schema::create('produsens', function(Blueprint $table)
		{
			$table->smallIncrements('id')->unsigned();
			$table->string('fullname', 100);
			$table->bigInteger('user_id')->unsigned();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('restrict')
				->onUpdate('cascade');
		});

		// create distributors
		Schema::create('distributors', function (Blueprint $table)
		{
			$table->smallIncrements('id')->unsigned();
			$table->string('fullname', 100);
			$table->bigInteger('user_id')->unsigned();
			$table->smallInteger('produsen_id')->unsigned();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('restrict')
				->onUpdate('cascade');
			$table->foreign('produsen_id')->references('id')->on('produsens')
				->onDelete('restrict')
				->onUpdate('cascade');
		});

		// create agents
		Schema::create('agents', function (Blueprint $table) {
			$table->smallIncrements('id')->unsigned();
			$table->string('fullname', 100);
	        $table->bigInteger('user_id')->unsigned();
			$table->smallInteger('distributor_id')->unsigned();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('restrict')
				->onUpdate('cascade');
			$table->foreign('distributor_id')->references('id')->on('distributors')
				->onDelete('restrict')
				->onUpdate('cascade');
		});

		// create product units
		Schema::create('product_units', function (Blueprint $table) {
			$table->smallIncrements('id')->unsigned();
			$table->string('name');
			$table->timestamps();
		});

		// create products
		Schema::create('products', function (Blueprint $table) {
			$table->smallIncrements('id')->unsigned();
			$table->string('name');
			$table->smallInteger('unit_id')->unsigned();
			$table->string('price')->nullable();
			$table->string('stock')->nullable();
			$table->timestamps();

			$table->foreign('unit_id')->references('id')->on('product_units')
				->onDelete('restrict')
				->onUpdate('cascade');
		});

		// create transactions
		Schema::create('transactions', function (Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('invoice_no', 25);
			$table->timestamp('tgl_order')->nullable();
			$table->string('total_payment');
			$table->text('note');
			$table->string('payment');
			$table->boolean('status')->default(0);
			$table->bigInteger('user_id')->unsigned();
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('restrict')
				->onUpdate('cascade');
		});

		// create detail transactions
		Schema::create('detail_transactions', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->smallInteger('product_id')->unsigned();
			$table->integer('transaction_id')->unsigned();
			$table->string('qty');
			$table->timestamps();

			$table->foreign('product_id')->references('id')->on('products')
				->onDelete('restrict')
				->onUpdate('cascade');
			$table->foreign('transaction_id')->references('id')->on('transactions')
				->onDelete('restrict')
				->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::dropIfExists('produsens');
		Schema::dropIfExists('customers');
		Schema::dropIfExists('distributors');
		Schema::dropIfExists('agents');
		Schema::dropIfExists('transactions');
		Schema::dropIfExists('detail_transactions');
		Schema::dropIfExists('product_units');
		Schema::dropIfExists('products');
	}
}