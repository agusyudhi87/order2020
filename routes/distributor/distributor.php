<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\WorkingpermitController;
use App\Http\Controllers\Backend\TransactionController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\UserManagementController;
use App\Http\Controllers\Backend\DistributorController;
use Arcanedev\LogViewer\Http\Controllers\LogViewerController;



// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');


Route::get('list', [DistributorController::class, 'list'])->name('list');

//transaction
Route::get('transaction', [TransactionController::class, 'index'])->name('transaction.index');
Route::get('transaction/list', [TransactionController::class, 'list'])->name('transaction.list');
Route::post('transaction/add', [TransactionController::class, 'save'])->name('transaction.save');
Route::post('transaction/edit/{idtransaction}', [TransactionController::class, 'update'])->name('transaction.update');
Route::post('transaction/update', [TransactionController::class, 'update'])->name('transaction.update');
Route::post('transaction/delete/{idtransactiondetail}', [TransactionController::class, 'delete'])->name('transactiondetail.delete');
Route::post('transaction/delete/all/{idtransaction}', [TransactionController::class, 'alldelete'])->name('transaction.delete');

//product
Route::get('product', [ProductController::class, 'index'])->name('product.index');
Route::get('product/list', [ProductController::class, 'list'])->name('product.list');
Route::post('product/add', [ProductController::class, 'save'])->name('product.save');
Route::post('product/edit/{idroom}', [ProductController::class, 'update'])->name('product.update');
Route::post('product/delete/{idroom}', [ProductController::class, 'delete'])->name('product.delete');
Route::get('product/get/courseplan', [ProductController::class, 'courseplan'])->name('product.get.courseplan');

//customer
Route::get('customer/list', [UserManagementController::class, 'list'])->name('customer.list');

Route::get('/perintah/{name}', function($name)
{
    Artisan::call('make:repository', ['name' => 'Backend\\'.$name.'Repository']);
    Artisan::call('make:controller', ['name' => 'Backend\\'.$name.'Controller']);
    Artisan::call('make:request', ['name' => 'Backend\\'.$name.'Request']);
});