@extends('backend.layouts.app-modern')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
  <div v-if="showTableKRS">
      <table-rencanastudi @data-krs="getDataMatkul" @data-krs="getDataKRS" :datakrs="choosedMatkulKRS"></table-rencanastudi>
  </div>
  <div v-if="showTableListMatkul">
      <table-matkul @data-krs="getDataMatkul" :datakrs="choosedMatkulKRS" ></table-matkul>
  </div>
  <div v-if="showDisplayKRS">
      <display-rencanastudi :datakrs="dataKRS"></display-rencanastudi>
  </div>

@endsection

@section('pagespecificscripts')
    {!! script(mix('js/rencanastudistudent.js')) !!}
@stop