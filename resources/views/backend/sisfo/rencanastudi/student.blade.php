@extends('backend.layouts.app-modern')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')



        <nav class="page-breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Tables</a></li>
            <li class="breadcrumb-item active" aria-current="page">Basic Tables</li>
          </ol>
        </nav>

        <div class="">
              <div v-if="showListKRS">
                  <list-krs @data-krs="getDataKRS"></list-krs>
              </div>  

              <div v-if="showDetailKRS">
                  <detail-krs @data-krs="getDataKRS" :datakrs="choosedKRS"></detail-krs>
              </div>

              <div v-if="showMatkulKRS">
                  <table-matkul @data-matkul="getDataKRS" :datakrs="choosedKRS"></table-matkul>
              </div>  

              <div v-if="showViewKRS">
                  <view-krs :datakrs="choosedKRS"></view-krs>
              </div>
        </div>









@endsection

@section('pagespecificscripts')
    {!! script(mix('js/rencanastudistudent.js')) !!}
@stop