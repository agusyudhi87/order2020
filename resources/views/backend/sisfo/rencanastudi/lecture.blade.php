@extends('backend.layouts.app-modern')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
  <div v-if="showListKRS">
      <list-krs @data-krs="getDataKRS"></list-krs>
  </div>  

  <div v-if="showDetailKRS">
      <detail-krs @data-krs="getDataKRS" :datakrs="choosedKRS"></detail-krs>
  </div>

  <div v-if="showMatkulKRS">
      <table-matkul @data-matkul="getDataKRS" :datakrs="choosedKRS"></table-matkul>
  </div>

@endsection

@section('pagespecificscripts')
    {!! script(mix('js/rencanastudiadmin.js')) !!}
@stop