@extends('backend.layouts.app-modern')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
  <div v-if="showListAll">
      <list-all @data-table="getData"></list-all>
  </div>  

  <div v-if="showFormAdd">
      <form-add @data-prop="getData"></form-add>
  </div>

  <div v-if="showFormEdit">
      <form-edit @data-prop="getData" :dataprop="choosedData"></form-edit>
  </div>


@endsection

@section('pagespecificscripts')
    {!! script(mix('js/room.js')) !!}
@stop