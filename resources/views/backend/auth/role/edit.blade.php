@extends('backend.layouts.app')

@section('title', __('labels.backend.access.roles.management') . ' | ' . __('labels.backend.access.roles.edit'))

@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

    <div class="row">
        <div class="col-xl-12 col-lg-12 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">

                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
{{ html()->modelForm($role, 'PATCH', route('admin.auth.role.update', $role))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.roles.management')
                        <small class="text-muted">@lang('labels.backend.access.roles.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <!--row-->

            <hr />

            <div class="row mt-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.roles.name'))
                            ->class('col-md-2 form-control-label')
                            ->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.roles.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.roles.associated_permissions'))
                            ->class('col-md-2 form-control-label')
                            ->for('permissions') }}

                        <div class="col-md-10">
                            @if($permissions->count())
                                @foreach($permissions as $permission)
                                    <div class="checkbox d-flex align-items-center">
                                        <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                            {{ html()->label(
                                                    html()->checkbox('permissions[]', in_array($permission->name, $rolePermissions), $permission->name)
                                                            ->class('switch-input')
                                                            ->id('permission-'.$permission->id)
                                                        .html()->label(ucwords($permission->name))->for('permission-'.$permission->id). '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                    ->class('kt-checkbox kt-checkbox--tick kt-checkbox--success')
                                                ->for('permission-'.$permission->id) }}
                                            
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.role.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
                    <!--end: Datatable -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
