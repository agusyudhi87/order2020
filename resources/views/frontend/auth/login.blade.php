@extends('frontend.layouts.login')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')

                  <div class="auth-form-wrapper px-4 py-5">
                    <a href="#" class="noble-ui-logo d-block mb-2">Sisfo<span>Management</span></a>
                    <h5 class="text-muted font-weight-normal mb-4">@lang('labels.frontend.auth.login_box_title')</h5>
                     {{ html()->form('POST', route('frontend.auth.login.post'))->class('forms-sample')->open() }}

                      <div class="form-group">

                        {{ html()->label(__('validation.attributes.frontend.email'))
                                                      ->for('email') }}

                        {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.email'))
                                ->attribute('maxlength', 191)
                                ->required() }}

                      </div>
                      <div class="form-group">
                         {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                                            {{ html()->password('password')
                                                    ->class('form-control')
                                                    ->placeholder(__('validation.attributes.frontend.password'))
                                                    ->required() }}

                        
                      </div>
                      <div class="form-check form-check-flat form-check-primary">
<!--                         <label class="form-check-label">
                          <input type="checkbox" class="form-check-input">
                          Remember me
                        </label> -->

                         {{ html()->label(html()
                                ->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                      </div>
                      <div class="mt-3">
                        {{ form_submit(__('labels.frontend.auth.login_button')) }}
                        <!-- <a href="../../dashboard-one.html" class="btn btn-primary mr-2 mb-2 mb-md-0 text-white">Login</a> -->
                        <button type="button" class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                          <i class="btn-icon-prepend" data-feather="twitter"></i>
                          Login with twitter
                        </button>
                      </div>
                      <a href="register.html" class="d-block mt-3 text-muted">Not a user? Sign up</a>
                      {{ html()->form()->close() }}
                  </div>



@endsection

@push('after-scripts')
    @if(config('access.captcha.login'))
        @captchaScripts
    @endif
@endpush
