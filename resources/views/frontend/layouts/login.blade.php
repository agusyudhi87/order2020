<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.nobleui.com/html/template/demo_1/pages/auth/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Jun 2020 15:22:21 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', app_name())</title>
    <!-- core:css -->
    <link rel="stylesheet" href="{{asset('display')}}/assets/vendors/core/core.css">
    <!-- endinject -->
  <!-- plugin css for this page -->
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('display')}}/assets/fonts/feather-font/css/iconfont.css">
    <link rel="stylesheet" href="{{asset('display')}}/assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <!-- endinject -->
  <!-- Layout styles -->  
    <link rel="stylesheet" href="{{asset('display')}}/assets/css/demo_1/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="{{asset('display')}}/assets/images/favicon.png" />
</head>
<body>
    <div class="main-wrapper">
        <div class="page-wrapper full-page">
            <div class="page-content d-flex align-items-center justify-content-center">

                <div class="row w-100 mx-0 auth-page">
                    <div class="col-md-8 col-xl-6 mx-auto">
                        <div class="card">
                            <div class="row">
                <div class="col-md-4 pr-md-0">
                  <div class="auth-left-wrapper">

                  </div>
                </div>
                <div class="col-md-8 pl-md-0">
                   @yield('content')  

                </div>
              </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- core:js -->
    <script src="{{asset('display')}}/assets/vendors/core/core.js"></script>
    <!-- endinject -->
  <!-- plugin js for this page -->
    <!-- end plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('display')}}/assets/vendors/feather-icons/feather.min.js"></script>
    <script src="{{asset('display')}}/assets/js/template.js"></script>
    <!-- endinject -->
  <!-- custom js for this page -->
    <!-- end custom js for this page -->
</body>

<!-- Mirrored from www.nobleui.com/html/template/demo_1/pages/auth/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 Jun 2020 15:22:21 GMT -->
</html>