
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';

window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('list-krs', require('./components/ListRencanaStudi.vue').default);
Vue.component('detail-krs', require('./components/DetailRencanaStudi.vue').default);
Vue.component('table-matkul', require('./components/TableMatkul.vue').default);
Vue.component('view-krs', require('./components/ViewRencanaStudi.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        dataKRS:{},
        showListKRS:true,
        showDetailKRS:false,
        showMatkulKRS:false,
        showViewKRS:false,
    },
    methods: {// receives a place object via the autocomplete component
                
        getDataKRS(value){
            console.log(value)
            if(value.action=='listKRS'){
                this.showListKRS=true
                this.showDetailKRS=false
                this.showMatkulKRS=false
                this.showViewKRS=false
            }

            if(value.action=='detailKRS'){
                this.showListKRS=false
                this.showDetailKRS=true
                this.showMatkulKRS=false
                this.showViewKRS=false
            }

            if(value.action=='matkulKRS'){
                this.showListKRS=false
                this.showDetailKRS=false
                this.showMatkulKRS=true
                this.showViewKRS=false
            }

            if(value.action=='viewKRS'){
                this.showListKRS=false
                this.showDetailKRS=false
                this.showMatkulKRS=false
                this.showViewKRS=true
            }
            this.choosedKRS = value.data
        }

    }
});