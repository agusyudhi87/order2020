export default {
   data () {
      return {
        id:'',
        rencanastudilist:[],
        studentcourseplanitem:[],
        studentcourseplan:{},
        // studentcourseplanitem:[]

      }

   },
   methods: {
         

      getDataStudent() {
          let self = this
          axios.get("/student/info").then(function(response) {
                //console.log(response.data)
                self.rencanastudilist = response.data
          })
      },         
      approveKRSMixin(data) {
        data.courseplanitem = JSON.stringify(data.courseplanitem)
        data.student = JSON.stringify(data.student)
        data.courseplan = JSON.stringify(data.courseplan)
        data.studentcourseplan = JSON.stringify(data.studentcourseplan)
          var formData = this.formData(data);
          axios.post("/student/krs/approve",formData).then(function(response) {
                self.studentcourseplanitem = response.data
          })
      },  
      formData(obj){
          var formData = new FormData();
          for (var key in obj) {
              formData.append(key, obj[key]);
          }
          return formData;
      },   

      deleteMatkulKRSMixin(data) {
        var formData = this.formData(data)
        axios.post("/student/krs/delete",formData).then(function(response) {
              self.studentcourseplanitem = response.data
        })
      },    

      saveCoursePlanMixin(data) {
        // data = JSON.stringify(data)
        let self = this
        data.courseplanitem = JSON.stringify(data.courseplanitem)
        // data.courseplanitemNew = JSON.stringify(data.courseplanitem)
        data.student = JSON.stringify(data.student)
        data.courseplan = JSON.stringify(data.courseplan)
        var formData = this.formData(data)
        axios.post("/student/krs/save",formData).then(function(response) {
              self.studentcourseplan = response.data
        })
      } 
  } 
}