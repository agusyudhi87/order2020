export default {
   data () {
      return {
        id:'',

      }

   },
   methods: {    
      approveKRSMixin(data) {
          
          var formData = this.formData(data);
          axios.post("/lecture/krs/approve",formData).then(function(response) {
                    self.optionmatkul = response.data

          })
      },  
      formData(obj){
          var formData = new FormData();
          for (var key in obj) {
              formData.append(key, obj[key]);
          }
          return formData;
      },   

      deleteMatkulKRSMixin(data) {
        var formData = this.formData(data)
        axios.post("/lecture/krs/delete",formData).then(function(response) {
              self.optionmatkul = response.data
        })
      },    

      addMatkulKRSMixin(data) {
        // data = JSON.stringify(data)
        data.courseplanitem = JSON.stringify(data.courseplanitem)
        data.student = JSON.stringify(data.student)
        var formData = this.formData(data)
        axios.post("/lecture/krs/save",formData).then(function(response) {
              self.optionmatkul = response.data
        })
      },
      saveCoursePlanMixin(data) {
        // data = JSON.stringify(data)
        let self = this
        data.courseplanitem = JSON.stringify(data.courseplanitem)
        // data.courseplanitemNew = JSON.stringify(data.courseplanitem)
        data.student = JSON.stringify(data.student)
        data.courseplan = JSON.stringify(data.courseplan)
        var formData = this.formData(data)
        axios.post("/lecture/krs/save",formData).then(function(response) {
              self.studentcourseplan = response.data
        })
      } 
  } 
}