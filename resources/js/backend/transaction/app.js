
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '../../bootstrap';
import '../../plugins';
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import VueSweetalert2 from 'vue-sweetalert2';
import Multiselect from 'vue-multiselect';
import VueMoment from 'vue-moment';
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';

import VueClockPicker from '@pencilpix/vue2-clock-picker';
import '@pencilpix/vue2-clock-picker/dist/vue2-clock-picker.min.css';
import Dayjs from 'vue-dayjs';



window.Vue = Vue;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(Dayjs)
Vue.use(VueSweetalert2)
Vue.use(VueMoment);
Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors"
});

Vue.component('vue-clock-picker', VueClockPicker );
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component('multiselect', Multiselect)
Vue.component('list-all', require('./components/Table.vue').default);
Vue.component('form-add', require('./components/FormAdd.vue').default);
Vue.component('form-edit', require('./components/FormEdit.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        choosedData:{},
        showListAll:true,
        showFormAdd:false,
        showFormEdit:false,
    },
    methods: {// receives a place object via the autocomplete component
                
        getData(value){
            // console.log(value)
            if(value.action=='list'){
                this.showListAll=true
                this.showFormAdd=false
                this.showFormEdit=false
            }else if(value.action=='add'){
                this.showListAll=false
                this.showFormAdd=true
                this.showFormEdit=false
            }else if(value.action=='edit'){
                this.showListAll=false
                this.showFormAdd=false
                this.showFormEdit=true
            }
            this.choosedData = value.data
        }

    }
});