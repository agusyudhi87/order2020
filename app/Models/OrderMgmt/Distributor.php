<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\Produsen;
use App\Models\OrderMgmt\Agent;

class Distributor extends Model
{
    protected $table = 'distributors';

    /**
     * Distributor has many Produsen.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function produsen()
    {
    	return $this->belongsTo(Produsen::class,'produsen_id','id');
    }   
    
    public function agent()
    {
    	return $this->hasMany(Agent::class,'distributor_id','id');
    }

    /**
     * Distributor belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }  
}
