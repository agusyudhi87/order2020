<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\Distributor;

class Agent extends Model
{
    protected $table = 'agents';

    /**
     * Agent has many Produsen.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function produsen()
    {
    	return $this->belongTo(Distributor::class,'produsen_id','id');
    }

    /**
     * Agent belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }  
}
