<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\TransactionDetail;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = ['invoice_no','tgl_order','total_payment','status','payment','user_id','note','customer_id'];

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id');
    }

    /**
     * Transaction belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
    	return $this->belongsTo(User::class,'customer_id');
    }

    public function transactiondetail()
    {
        return $this->hasMany(TransactionDetail::class,'transaction_id');
    }


}
