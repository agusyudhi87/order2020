<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\Distributor;

class Produsen extends Model
{
    protected $table = 'produsens';

    /**
     * Produsen has many Distributor.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function distributor()
    {
    	return $this->hasMany(Distributor::class,'produsen_id','id');
    }

    
    /**
     * Produsen belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }  
}
