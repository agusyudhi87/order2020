<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\Distributor;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['id','name','unit_id','price','stock'];

    /**
     * Produsen belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }  
}
