<?php

namespace App\Models\OrderMgmt;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User;
use App\Models\OrderMgmt\Transaction;
use App\Models\OrderMgmt\Product;

class TransactionDetail extends Model
{
    protected $table = 'transaction_details';
    protected $fillable = ['product_id','transaction_id','qty'];

    public function user()
    {
    	return $this->belongsToMany(Product::class,'id');
    }

    /**
     * TransactionDetail belongs to User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
    	return $this->belongsTo(Product::class,'product_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class,'id');
    }


}
