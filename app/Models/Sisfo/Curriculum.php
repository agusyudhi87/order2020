<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CurriculumPlan;
use App\Models\Sisfo\Program;

class Curriculum extends Model
{
    protected $table = 'curriculums';

    public function curriculumplan(){
        return $this->hasMany(CurriculumPlan::class,'curriculum_id');
    }   
    public function program(){
        return $this->belongsTo(Program::class,'program_id');
    }   
}
