<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\Course;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\StudentCoursePlan;


class CoursePlanItem extends Model
{
    protected $table = 'course_plan_items';

    /**
     * CoursePlanItem belongs to Courseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseplan()
    {
    	// belongsTo(RelatedModel, foreignKey = courseplan_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Courseplan::class);
    }

    /**
     * CoursePlanItem belongs to Course.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
    	// belongsTo(RelatedModel, foreignKey = course_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Course::class,'course_id');
    }

    /**
     * CoursePlanItem belongs to Lecture.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lecture()
    {
    	// belongsTo(RelatedModel, foreignKey = lecture_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Employee::class,'lecturer_id','code');
    }


    public function slot()
    {

    	return $this->belongsToMany(Slot::class,'course_schedule','course_plan_item_id','slot_id');
    }


    public function studentcourseplan(){
        return $this->belongsToMany(StudentCoursePlan::class,'student_course_plan_items','student_course_plan_id','course_plan_item_id')->withPivot('grade','presence')->withTimestamps();
    }    



}
