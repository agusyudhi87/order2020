<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CurriculumsPlan;
use App\Models\Sisfo\CoursePlanItem;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['code','name','credit'];

    public function curriculumplan(){
        return $this->belongsToMany(CurriculumsPlan::class, 'curriculum_plan_details', 'curriculum_plan_id', 'course_id')->withPivot('is_mandatory');

        //untuk akses pivot
        // $model->curriculumplan()->where('curriculum_plan_id', $curriculumplan->id)->first()->pivot->is_mandatory
    }    
    
    public function courseplanitem(){
        return $this->hasMany(CoursePlanItem::class, 'course_id');
    }    


}
