<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Program;
use App\Models\Sisfo\Room;

class Faculty extends Model
{
   protected $table = 'faculties';

   /**
    * Faculty has many Program.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function program()
   {
   		// hasMany(RelatedModel, foreignKeyOnRelatedModel = faculty_id, localKey = id)
   		return $this->hasMany(Program::class,'faculty_id');
   }

   /**
    * Faculty has many Room.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function room()
   {
   	// hasMany(RelatedModel, foreignKeyOnRelatedModel = faculty_id, localKey = id)
   	return $this->hasMany(Room::class,'owner');
   }
}
