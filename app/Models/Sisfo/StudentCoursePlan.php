<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\Employee;
use App\Models\Sisfo\CoursePlanItem;

class StudentCoursePlan extends Model
{
    protected $table = 'student_course_plans';
    protected $fillable = ['course_plan_id','student_id','semester','total_credit','gpa','status','approved_by'];

    public function courseplan(){
        return $this->belongsTo(CoursePlan::class,'course_plan_id');
    }    

    /**
     * StudentCoursePlan belongs to Student.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        // belongsTo(RelatedModel, foreignKey = student_id, keyOnRelatedModel = id)
        return $this->belongsTo(Student::class,'student_id','code');
    }

    public function employee(){
        return $this->belongsTo(Employee::class,'approved_by');
    }

 	public function courseplanitem(){
        return $this->belongsToMany(CoursePlanItem::class,'student_course_plan_items','student_course_plan_id','course_plan_item_id')->withPivot('grade','presence')->withTimestamps();
    }       


}
