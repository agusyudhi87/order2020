<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;

use App\Models\Sisfo\StudentCoursePlan;

class Employee extends Model
{
   protected $table = 'employees';

   /**
    * Employee has many Studentcourseplan.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function studentcourseplan()
   {
   		// hasMany(RelatedModel, foreignKeyOnRelatedModel = employee_id, localKey = id)
   		return $this->hasMany(StudentCoursePlan::class,'lecturer_id','code');
   }

}
	