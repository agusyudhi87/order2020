<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\CoursePlanItem;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\Room;

class Slot extends Model
{
    protected $table = 'slots';
    protected $fillable = ['course_plan_id','room_id','day_index','start_time','end_time','status','note'];

    public function courseplanitem()
    {

    	return $this->belongsToMany(CoursePlanItem::class,'course_schedule','slot_id','course_plan_item_id');
    }

    /**
     * Slot belongs to Courseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseplan()
    {
    	// belongsTo(RelatedModel, foreignKey = courseplan_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Courseplan::class,'course_plan_id');
    }

    /**
     * Slot belongs to Room.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
    	// belongsTo(RelatedModel, foreignKey = room_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Room::class,'room_id');
    }
}
