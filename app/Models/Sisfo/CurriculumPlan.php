<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Course;
use App\Models\Sisfo\Curriculum;

class CurriculumPlan extends Model
{
    protected $table = 'curriculum_plans';

    public function course(){
        return $this->belongsToMany(Course::class, 'curriculums_plan_details', 'curriculum_plan_id', 'course_id')->withPivot('is_mandatory');
    }    

    public function curriculum(){
        return $this->belongsTo(Curriculum::class,'curriculum_id');
    }    


}
