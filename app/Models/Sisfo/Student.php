<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\StudentCoursePlan;
use App\Models\Sisfo\Program;
use App\Models\Auth\User;
use App\Models\Sisfo\CoursePlanItem;

class Student extends Model
{
    protected $table = 'students';

    /**
     * Student has many Studentcourseplan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function studentcourseplan()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = student_id, localKey = id)
    	return $this->hasMany(StudentCoursePlan::class,'student_id','code');
    }

    /**
     * Student belongs to Program.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program()
    {
    	// belongsTo(RelatedModel, foreignKey = program_id, keyOnRelatedModel = id)
    	return $this->belongsTo(Program::class,'program_id');
    }


    public function courseplanitem(){
        return $this->belongsToMany(CoursePlanItem::class, 'course_attendance_details','student_id', 'course_plan_id');
    }  

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }  



}
