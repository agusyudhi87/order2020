<?php

namespace App\Models\Sisfo;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sisfo\Curriculum;
use App\Models\Sisfo\Student;
use App\Models\Sisfo\CoursePlan;

class Program extends Model
{
   protected $table = 'programs';

   //hidden ID
   // protected $hidden = ['id'];

   /**
    * Program has many Curriculum.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function curriculum()
   {
   		// hasMany(RelatedModel, foreignKeyOnRelatedModel = program_id, localKey = id)
   		return $this->hasMany(Curriculum::class,'program_id');
   }

   /**
    * Program has many Student.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function student()
   {
   	// hasMany(RelatedModel, foreignKeyOnRelatedModel = program_id, localKey = id)
   		return $this->hasMany(Student::class,'program_id');
   }

   /**
    * Program belongs to Faculty.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
   public function faculty()
   {
     // belongsTo(RelatedModel, foreignKey = faculty_id, keyOnRelatedModel = id)
     return $this->belongsTo(Faculty::class,'faculty_id');
   }

   /**
    * Program has many Courseplan.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function courseplan()
   {
   	// hasMany(RelatedModel, foreignKeyOnRelatedModel = program_id, localKey = id)
   	return $this->hasMany(CoursePlan::class,'program_id');
   }

}
