<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\PasswordHistory;
use App\Models\Auth\SocialAccount;
use App\Models\Sisfo\Employee;
use App\Models\OrderMgmt\Distributor;
use App\Models\OrderMgmt\Agent;
use App\Models\OrderMgmt\Produsen;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class);
    }

    /**
     * UserRelationship has one Employee.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = userRelationship_id, localKey = id)
        return $this->hasOne(Employee::class,'user_id');
    }


    public function distributor()
    {
        return $this->hasMany(Distributor::class,'user_id');
    }

    public function agent()
    {
        return $this->hasMany(Agent::class,'user_id');
    }

    public function produsen()
    {
        return $this->hasMany(Produsen::class,'user_id');
    }
}
