<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CoursePlanRequest;
use App\Repositories\Backend\CoursePlanRepository;
// use App\Repositories\Backend\StudentCoursePlanRepository;

class CoursePlanController extends Controller
{
    public function __construct(CoursePlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('backend.sisfo.courseplan.index');
    }

    public function list(){
        return  $this->repository->get();         
    }
    
    public function save(CourseplanRequest $request)
    {
       return $this->repository->create($request->all());
    }

    public function update(CourseplanRequest $request, String $id)
    {
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    
}
