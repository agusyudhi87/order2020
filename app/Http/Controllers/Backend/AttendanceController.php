<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\AttendanceRequest;
use App\Repositories\Backend\AttendanceRepository;

class AttendanceController extends Controller
{
    public function __construct(AttendanceRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.Attendance.index');
    }

    public function list(){
        return  $this->repository->get();         
    }

    public function save(AttendanceRequest $request)
    {
        // dd($request->all());
       return $this->repository->create($request->all());
    }

    public function update(AttendanceRequest $request, String $id)
    {
        // dd($request->all());
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($request->all());
    }    

}
