<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CourseRequest;
use App\Repositories\Backend\CourseRepository;

class CourseController extends Controller
{
    public function __construct(CourseRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.course.index');
    }

    public function list(){
        return  $this->repository->get();         
    }

    public function save(CourseRequest $request)
    {
        // dd($request->all());
       return $this->repository->create($request->all());
    }

    public function update(CourseRequest $request, String $id)
    {

       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    

}
