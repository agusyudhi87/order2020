<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ProgramRequest;
use App\Repositories\Backend\ProgramRepository;

class ProgramController extends Controller
{
    public function __construct(ProgramRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.program.index');
    }

    public function list(){
        return  $this->repository->get();         
    }

    public function save(ProgramRequest $request)
    {
       // dd($request->all());
       return $this->repository->create($request->all());
    }

    public function update(ProgramRequest $request, String $id)
    {
        // dd($id);
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    

}
