<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SlotRequest;
use App\Repositories\Backend\SlotRepository;

class SlotController extends Controller
{
    public function __construct(SlotRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.slot.index');
    }

    public function list(){
        return  $this->repository->get();         
    }    

    
    public function save(SlotRequest $request)
    {
        // ada preprocessing di form request
       return $this->repository->create($request->all());
    }

    public function update(SlotRequest $request, String $id)
    {
       // ada preprocessing di form request
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    


}
