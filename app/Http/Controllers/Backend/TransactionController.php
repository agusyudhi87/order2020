<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\TransactionRequest;
use App\Repositories\Backend\TransactionRepository;

class TransactionController extends Controller
{
    public function __construct(TransactionRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.order.transaction.index');
    }

    public function list(){
        return  $this->repository->get();         
    }    

    public function save(TransactionRequest $request)
    {
        // ada preprocessing di form request
       return $this->repository->create($request->all());
    }

    public function update(TransactionRequest $request)
    {
       // ada preprocessing di form request
       return $this->repository->update($request->all());
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }  
    
    public function alldelete(String $id)
    {
       return $this->repository->alldelete($id);
    }    
}
