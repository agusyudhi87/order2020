<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ProdusenRequest;
use App\Repositories\Backend\ProdusenRepository;

class ProdusenController extends Controller
{
    public function __construct(ProdusenRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.order.transaction.index');
    }

    public function list(){
        return  $this->repository->get();         
    }    

    public function save(ProdusenRequest $request)
    {
        // ada preprocessing di form request
       return $this->repository->create($request->all());
    }

    public function update(ProdusenRequest $request)
    {
       // ada preprocessing di form request
       return $this->repository->update($request->all());
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }  
    
    public function alldelete(String $id)
    {
       return $this->repository->alldelete($id);
    }    
}
