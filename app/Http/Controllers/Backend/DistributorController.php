<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\DistributorRequest;
use App\Repositories\Backend\DistributorRepository;

class DistributorController extends Controller
{
    public function __construct(DistributorRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.order.transaction.index');
    }

    public function list(){
        return  $this->repository->get();         
    }    

    public function save(DistributorRequest $request)
    {
        // ada preprocessing di form request
       return $this->repository->create($request->all());
    }

    public function update(DistributorRequest $request)
    {
       // ada preprocessing di form request
       return $this->repository->update($request->all());
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }  
    
    public function alldelete(String $id)
    {
       return $this->repository->alldelete($id);
    }    
}
