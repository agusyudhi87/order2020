<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ProductRequest;
use App\Repositories\Backend\ProductRepository;

class ProductController extends Controller
{
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.order.product.index');
    }

    public function list(){
        return  $this->repository->get();         
    }    

    
    public function save(ProductRequest $request)
    {
        // ada preprocessing di form request
       return $this->repository->create($request->all());
    }

    public function update(ProductRequest $request)
    {
       // ada preprocessing di form request
       return $this->repository->update($request->all());
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    
}
