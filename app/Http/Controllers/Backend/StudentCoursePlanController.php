<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\StudentCoursePlanRequest;
// use App\Repositories\Backend\StudentplanRepository;
use App\Repositories\Backend\StudentCoursePlanRepository;

class StudentCoursePlanController extends Controller
{
    public function __construct(StudentCoursePlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function planmatakuliah($idprogram)
    {
        $data =  $this->repository->showplanmatkul($idprogram); 
        //Map Course Plan Item insert selected false
        $courseplanitem =  $data->first()
                                ->courseplanitem
                                ->map(function ($item,$key) {
                                	$item->selected = false;
                                    $item->disable = false;
                        		    return $item;
                        		});
		$data->courseplanitem = $courseplanitem;
        return $data->first(); 
    }

    public function getList(){
         $data =  $this->repository->get(); 
        //Map Course Plan Item insert selected true
         $courseplanitem =  $data->each(function ($item,$key) {
                                    $item = $item->courseplanitem->map(function ($datacp,$key) {
                                                $datacp->selected = true;
                                                $datacp->disable = false;
                                                return $datacp;
                                            });
                                    return $item;
                                });
        $data->courseplanitem = $courseplanitem;
        return $data;                         
    }

    //menampilkan relasi student dengan courseplanitem
    public function getStudentCourseplanItem(StudentCoursePlanRequest $request){
         $data =  $this->repository->getStudentCourseplanItem($request->all()); 
         // dd($data);
        //Map Course Plan Item insert selected true
        // $courseplanitem = [];
        if(!is_null($data)){

            $courseplanitem =  $data->each(function ($item,$key) {
                                    $item = $item->courseplanitem->map(function ($datacp,$key) {
                                                $datacp->selected = true;
                                                $datacp->disable = false;
                                                return $datacp;
                                            });
                                    return $item;
                                });
            $data->courseplanitem = $courseplanitem;
        }else{
            $data=null;
        }
        return $data;                         
    }

    public function krslist()
    {
         return view('backend.sisfo.rencanastudi.lecture');       
    }

    public function index()
    {
        //echo "masuk";
        return view('backend.sisfo.rencanastudi.student');
    }

    public function approveLecture(StudentCoursePlanRequest $request)
    {
        return $this->repository->approveLecture($request->all());
    }    

    public function approveStudent(StudentCoursePlanRequest $request)
    {
        return $this->repository->approveStudent($request->all());
    }    

    public function delete(StudentCoursePlanRequest $request)
    {
       return $this->repository->delete($request->all());
    }    

    public function save(StudentCoursePlanRequest $request)
    {
        // dd($request->all());
       return $this->repository->create($request->all());
    }
}
