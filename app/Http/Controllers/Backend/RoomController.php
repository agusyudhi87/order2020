<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\RoomRequest;
use App\Repositories\Backend\RoomRepository;

class RoomController extends Controller
{
    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.room.index');
    }

    public function list(){
        return  $this->repository->get();         
    }

    public function save(RoomRequest $request)
    {
       // dd($request->all());
       return $this->repository->create($request->all());
    }

    public function update(RoomRequest $request, String $id)
    {
        // dd($id);
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    

}
