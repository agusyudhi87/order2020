<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CoursePlanRequest;
use App\Repositories\Backend\CoursePlanRepository;
// use App\Repositories\Backend\StudentCoursePlanRepository;

class CoursePlanController extends Controller
{
    public function __construct(CoursePlanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function planmatakuliah($idprogram)
    {
        $data =  $this->repository->showplanmatkul($idprogram); 
        
        //reMapping Course Plan Item insert selected false
        $courseplanitem =  $data->first()
                                ->courseplanitem
                                ->map(function ($item,$key) {
                                	$item->selected = false;
                        		    return $item;
                        		});

		$data->courseplanitem = $courseplanitem;
        return $data->first(); 
       
    }    

    public function krslist()
    {
        $data =  $this->repository->krslist(); 
        
        //reMapping Course Plan Item insert selected false
        $courseplanitem =  $data->first()
                                ->courseplanitem
                                ->map(function ($item,$key) {
                                    $item->selected = false;
                                    return $item;
                                });

        $data->courseplanitem = $courseplanitem;
        return $data->first(); 
       
    }    


    public function list()
    {
         return view('backend.sisfo.rencanastudi.list');       
    }

    public function index()
    {
        //echo "masuk";
        return view('backend.sisfo.rencanastudi.index');
    }

    public function save(CoursePlanRequest $request)
    {
        //echo "masuk";
        return $this->repository->createStudentCoursePlan($request->all());
    }
}
