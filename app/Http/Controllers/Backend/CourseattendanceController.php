<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\CourseattendanceRequest;
use App\Repositories\Backend\CourseattendanceRepository;

class CourseattendanceController extends Controller
{
    public function __construct(CourseattendanceRepository $repository)
    {
        $this->repository = $repository;
    }
    public function index()
    {
        return view('backend.sisfo.Courseattendance.index');
    }

    public function list(){
        return  $this->repository->get();         
    }

    public function save(CourseattendanceRequest $request)
    {
        // dd($request->all());
       return $this->repository->create($request->all());
    }

    public function update(CourseattendanceRequest $request, String $id)
    {
        // dd($request->all());
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($request->all());
    }    


}
