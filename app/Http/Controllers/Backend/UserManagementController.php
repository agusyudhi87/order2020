<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\OrderMgmt\Distributor;
use App\Models\OrderMgmt\Agent;
use App\Models\OrderMgmt\Produsen;
use App\Models\Auth\User;

class UserManagementController extends Controller
{
    public function list()
    {
        if (auth()->user()->hasRole(['produsen'])) {
            return User::whereHas("roles", function($q){ 
                    $q->where("name",'distributor');
                    $q->orWhere("name",'agent'); 
                })->get();
        }

        if (auth()->user()->hasRole(['distributor'])) {
            return User::whereHas("roles", function($q){ 
                $q->where("name",'distributor'); 
                $q->orWhere("name",'agent'); 
            })->get();
        }

        if (auth()->user()->hasRole(['agent'])) {
            return User::whereHas("roles", function($q){ 
                $q->where("name",'agent'); 
            })->get();
        }

        if (auth()->user()->hasRole(['administrator'])) {
            return User::get();
        }

    }
}
