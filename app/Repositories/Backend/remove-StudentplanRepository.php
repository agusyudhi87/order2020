<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\StudentCoursePlan;
use App\Models\Sisfo\CoursePlanItem;
use Carbon\Carbon;

//use Your Model

/**
 * Class CoursePlanRepository.
 */
class StudentplanRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    // public function __construct(CoursePlan $model)
    // {
    //     $this->model = $model;
    // } 

    public function showplanmatkul($idProgram)
    {
        return CoursePlan::with(['courseplanitem','courseplanitem.course','courseplanitem.lecture'])
                            ->where('program_id',$idProgram)
                            ->get();
    }

    public function get(){
          return StudentCoursePlan::has('courseplanitem')->get();                    
    }
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {

            $mytime = Carbon::now();
            $data['student'] = json_decode($data['student'],true);
            $data['matkulSelected'] = json_decode($data['matkulSelected'],true);

            if(typeof $data['studentcourseplanid']=='undefined'){
                //Save STUDENT COURSE PLAN
                $studentcourseplan = new StudentCoursePlan;
                $studentcourseplan->course_plan_id = $data['courseplanid'];
                $studentcourseplan->student_id = $data['student']['code'];
                $studentcourseplan->semester = $data['semester'];
                $studentcourseplan->total_credit = $data['total_credit'];
                $studentcourseplan->gpa = $data['gpa'];
                $studentcourseplan->status = ($data['action']==='final')?2:1;
                $studentcourseplan->save();
            }else{
                $studentcourseplan = StudentCoursePlan::findOrFail($data['studentcourseplanid']); 
            }


            $dataSync = array();
            //Save STUDENT COURSE PLAN ITEM
            foreach ($data['matkulSelected'] as $matkulSelected) {                
                $dataSync[$matkulSelected['id']] = ['grade' => 0,'presence'=0];  
            }
            $studentcourseplan->courseplanitem()->sync($dataSync);
          
            // $users = User::whereHas('organizations', function ($q) {
            //     $q->where('name', 'like', '%someName%');  
            // })->get(); 

            //update capacity
            foreach($dataSync as $key => $value){ 
                  $studentcourseplan = CoursePlanItem::find($key); 
                  $studentcourseplan->capacity = $studentcourseplan->capacity - 1; 
                  $studentcourseplan->save(); 
            }

           
        });
    }


}
