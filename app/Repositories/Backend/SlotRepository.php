<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\Slot;
use App\Models\Sisfo\Courseplan;

//use Your Model

/**
 * Class SlotRepository.
 */
class SlotRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Slot $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
         $insertData=[
                "course_plan_id" => $data['courseplan']->id,
                "room_id" => $data['room']->id,
                "day_index" => $data['day_index']->id,
                "start_time" => $data['start_time'],
                "end_time" => $data['end_time'],
                "status" => $data['status'],
                "note" => 'goood'
            ];

        return DB::transaction(function () use ($insertData) {

            $model = $this->model::create($insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.backend.slot.create_error'));
        });

    }     

    public function get(){
        return $this->model::with(['courseplan.program.faculty','room'])->orderBy('created_at', 'desc')->get();
    }     


    public function update(array $data,string $id){
        $insertData=[
                "course_plan_id" => $data['courseplan']->id,
                "room_id" => $data['room']->id,
                "day_index" => $data['day_index']->id,
                "start_time" => $data['start_time'],
                "end_time" => $data['end_time'],
                "status" => $data['status'],
                "note" => 'goood'
            ];
        return DB::transaction(function () use ($insertData,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.backend.slot.update_error'));
        });
    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }
}
