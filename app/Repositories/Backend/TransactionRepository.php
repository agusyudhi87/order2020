<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\OrderMgmt\Transaction;
use App\Models\OrderMgmt\TransactionDetail;
use App\Models\Auth\User;

//use Your Model

/**
 * Class TransactionRepository.
 */
class TransactionRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
        // dd($data);
        $insertData=[
            "invoice_no" => $this->getNewInvoiceNo(),
            "tgl_order" => now(),
            "total_payment" => 0,
            "payment" => 0,
            "customer_id" => auth()->user()->id,
            "user_id" => auth()->user()->id,
        ];
        $product = json_decode($data['product'],true);
        $detailTransaction = [
            'qty'=> $data['qty'],
            'product_id'=> $product["id"]
        ];
        // dd($detailTransaction);


        return DB::transaction(function () use ($insertData, $detailTransaction) {

            $transaction =  $this->model::create($insertData);
            return $transaction->transactiondetail()->create($detailTransaction);                
            
            throw new GeneralException(__('exceptions.backend.Transaction.create_error'));
        });

    }     

    public function getNewInvoiceNo()
    {
        $noInvoice = date('m');

        $transaction = Transaction::max('invoice_no');
        if($transaction!="")
        {
            $tmp =((int)$transaction)+1;
            $length =sprintf("%06s", $tmp);        
            $noInvoice = $length;
        }
        else
        {
            $noInvoice = "0001";
        }   
        return $noInvoice;
    }

    public function get(){

        //ambil id user yang lagi login
        $idUserLogin = auth()->user()->id;
        $arrayId = [];
        //relasikan dengan modelnya sesuai role
    



        // foreach($user->first()->produsen()->get() as $produsen){
            
        // }

        // return ($user);
        // dd($user->first()->pluck('id'));
        //get array id

        //lalu tarik data dari array id

        $data = $this->model::with(['customer','transactiondetail.product'])->where('customer_id','2')->get();
        $data = $data->map(function($item){
            $total = 0;
            foreach($item->transactiondetail()->get() as $transaction){
                $total += $transaction->product()->get()->first()->price * $transaction->qty;
            }
            $item->total_item = $total;
            return $item;
        });
        // dd($data);
        return $data;
    }     


    public function update(array $data){
        $product = json_decode($data['product'],true);
        $detailTransaction = [
            'qty'=> $data['qty'],
            'product_id'=> $product["id"]
        ];

        $transaction = $this->model::find($data['idtransaction']);
        $transaction->transactiondetail()->create($detailTransaction);

        return $data;

    }

    public function delete(String $id){
        $model = TransactionDetail::find($id);
        // dd($model);
        $model->delete();
    }

    public function alldelete(String $id){
        $model = $this->model::with('transactiondetail')->find($id);
        // dd($model);
        $model->delete();
    }
}
