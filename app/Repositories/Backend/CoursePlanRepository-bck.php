<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlan;
use App\Models\Sisfo\StudentCoursePlan;
use App\Models\Sisfo\CoursePlanItem;
use Carbon\Carbon;

//use Your Model

/**
 * Class CoursePlanRepository.
 */
class CoursePlanRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CoursePlan $model)
    {
        $this->model = $model;
    } 

     public function index()
    {
        return view('backend.sisfo.slot.index');
    }

    public function list(){
        return  $this->repository->get();         
    }
    
    public function save(SlotRequest $request)
    {
       return $this->repository->create($request->all());
    }

    public function update(SlotRequest $request, String $id)
    {
       return $this->repository->update($request->all(),$id);
    }

    public function delete(String $id)
    {
       return $this->repository->delete($id);
    }    




    public function showplanmatkul($idProgram)
    {
        return $this->model::with(['courseplanitem','courseplanitem.course','courseplanitem.lecture'])
                            ->where('program_id',$idProgram)
                            ->get();
    }

    public function getStudentCoursePlan(){
        return StudentCoursePlan::with(['courseplanitem','courseplanitem.course','courseplanitem.lecture'])
                            ->where('program_id',$idProgram)
                            ->get();
    }
    public function createStudentCoursePlan(array $data)
    {
        return DB::transaction(function () use ($data) {

            $mytime = Carbon::now();
            $data['student'] = json_decode($data['student'],true);
            $data['matkulSelected'] = json_decode($data['matkulSelected'],true);

            //Save STUDENT COURSE PLAN
            $studentcourseplan = new StudentCoursePlan;
            $studentcourseplan->course_plan_id = $data['courseplanid'];
            $studentcourseplan->student_id = $data['student']['code'];
            $studentcourseplan->semester = $data['semester'];
            $studentcourseplan->total_credit = $data['total_credit'];
            $studentcourseplan->gpa = $data['gpa'];
            $studentcourseplan->status = ($data['action']==='final')?2:1;
            $studentcourseplan->save();

            $dataSync = array();
            //Save STUDENT COURSE PLAN ITEM
            foreach ($data['matkulSelected'] as $matkulSelected) {                
                $dataSync[$matkulSelected['id']] = ['grade' => 0,'presence'=0];  
            }
            $studentcourseplan->courseplanitem()->sync($dataSync);
          
            //update capacity
            foreach($dataSync as $key => $value){ 

                  $studentcourseplan = CoursePlanItem::find($key); 
                  $studentcourseplan->capacity = $studentcourseplan->capacity - 1; 
                  $studentcourseplan->save(); 
            }

           
        });
    }


}
