<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\CoursePlan;
use Carbon\Carbon;

//use Your Model

/**
 * Class CoursePlanRepository.
 */
class CoursePlanRepository extends BaseRepository
{
   
 /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(CoursePlan $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
        $insertData=[
            "year" => $data['year'],
            "semester" => $data['semester'],
            "program_id" => $data['program']->id,
            "status" => $data['status']
        ];
        return DB::transaction(function () use ($insertData) {
            $model = $this->model::create($insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });

    }     

    public function get(){
        return $this->model::with(['program'])->orderBy('created_at', 'desc')->get();
    }    

    public function update(array $data,string $id){
        $insertData=[
            "year" => $data['year'],
            "semester" => $data['semester'],
            "program_id" => $data['program']->id,
            "status" => $data['status']
        ];
        return DB::transaction(function () use ($insertData,$id) {
            $model = $this->model::updateOrCreate(['id' => $id],$insertData);
            
            return $model;                                    
            throw new GeneralException(__('exceptions.frontend.orders.update_error'));
        });
    }

    public function delete(String $id){
        $model = $this->model::find($id);
        $model->delete();
    }



}
