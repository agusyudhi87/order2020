<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\Sisfo\Student;

//use Your Model

/**
 * Class StudentRepository.
 */
class StudentRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Student $model)
    {
        $this->model = $model;
    } 

    public function getInfo($id=null)
    {
        $id = ($id==null)?auth()->user()->id:$id;

        $info = $this->model::with([
                    'program',
                    'program.faculty',
                    'program.courseplan',
                    'studentcourseplan.courseplanitem.lecture',
                    'studentcourseplan.courseplanitem.course',
                    'user'=> function($query) use ($id) {
                                $query->select(['id','first_name']);
                                $query->where('id',$id);
                            }])
                    ->where('user_id',$id)
                    ->get();

        // $info->studentcourseplan = $info;

        // $info = $info->map(function ($info) use ($id) {
        //             // $info->rencanastudilist = $id;
        //             return $info;
        //         });         
        return $info->first();
    }
}
