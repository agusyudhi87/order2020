<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\OrderMgmt\Produsen;
use App\Models\OrderMgmt\Transaction;
use App\Models\OrderMgmt\TransactionDetail;
use App\Models\Auth\User;

//use Your Model

/**
 * Class TransactionRepository.
 */
class AgentRepository extends BaseRepository
{
   /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    } 

    public function create(array $data){
      

    }     

    public function getNewInvoiceNo()
    {
     
    }

    public function get(){

        //ambil id user yang lagi login
        $idUserLogin = auth()->user()->id;
        $arrayId = [];
        //relasikan dengan modelnya sesuai role
        $user = User::with('agent.user')
                ->where('id', $idUserLogin)
                ->get();

        $user = $user->toArray();

        // dd($user_id);
        $idUser = [];
        foreach($user as $usr){
                foreach ($usr['agent'] as $agent) {
                    $idUser[] = $agent['user']['id'];

                }
        }

        $transaction = Transaction::with('user.roles')->whereIn('user_id',$idUser)->get();

        return ($transaction);

    
    }     


    public function update(array $data){

    }

    public function delete(String $id){

    }

    public function alldelete(String $id){

    }
}
