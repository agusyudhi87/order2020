<?php

namespace App\Repositories\Backend;

use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Models\OrderMgmt\Product;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    } 

    public function create(array $data)
    {
         $insertData=[
                "course_plan_id" => $data['courseplan']->id,
                "room_id" => $data['room']->id,
                "day_index" => $data['day_index']->id,
                "start_time" => $data['start_time'],
                "end_time" => $data['end_time'],
                "status" => $data['status'],
                "note" => 'goood'
            ];

        return DB::transaction(function () use ($insertData) {

            $model = $this->model::create($insertData);
            return $model;                                    
            throw new GeneralException(__('exceptions.backend.Product.create_error'));
        });

    }     

    public function get()
    {
        return $this->model::get();
    }     


    public function update(array $data)
    {
        $idProduct = $this->model::find($data['idproduct']);
        $stockAwal = $idProduct->stock;
        $updateData=[
            "stock" => $data['stock'] + $stockAwal,
        ];
        // dd($updateData);
        $idProduct->update($updateData);

        return $data;
    }

    public function delete(String $id)
    {
        $model = $this->model::find($id);
        $model->delete();
    }
}
