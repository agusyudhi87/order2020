<?php

if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */

    // Catatan Dode
    // home_route akan dipanggil middleware DHttp\Middleware\Authenticate
    function home_route()
    {

        // dd(auth()->user()->hasRole(['produsen']));
        if (auth()->check()) {
            if (auth()->user()->hasRole(['administrator'])) {
                 return 'admin.dashboard';
            }

            if (auth()->user()->hasRole(['produsen'])) {
                return 'produsen.dashboard';
            }

            if (auth()->user()->hasRole(['distributor'])) {
                return 'distributor.dashboard';
            }

            if (auth()->user()->hasRole(['agent'])) {
                return 'agent.dashboard';
            }

            //return 'frontend.user.dashboard';
        }

        return 'frontend.auth.login';
        

    }
}
